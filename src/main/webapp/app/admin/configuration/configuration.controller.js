(function() {
    'use strict';

    angular
        .module('dmtoolsApp')
        .controller('DMTConfigurationController', DMTConfigurationController);

    DMTConfigurationController.$inject = ['$filter','DMTConfigurationService'];

    function DMTConfigurationController (filter,DMTConfigurationService) {
        var vm = this;

        vm.allConfiguration = null;
        vm.configuration = null;

        DMTConfigurationService.get().then(function(configuration) {
            vm.configuration = configuration;
        });
        DMTConfigurationService.getEnv().then(function (configuration) {
            vm.allConfiguration = configuration;
        });
    }
})();
