(function() {
    'use strict';

    angular
        .module('dmtoolsApp')
        .factory('DMTMetricsService', DMTMetricsService);

    DMTMetricsService.$inject = ['$rootScope', '$http'];

    function DMTMetricsService ($rootScope, $http) {
        var service = {
            getMetrics: getMetrics,
            threadDump: threadDump
        };

        return service;

        function getMetrics () {
            return $http.get('management/dmtools/metrics').then(function (response) {
                return response.data;
            });
        }

        function threadDump () {
            return $http.get('management/dump').then(function (response) {
                return response.data;
            });
        }
    }
})();
