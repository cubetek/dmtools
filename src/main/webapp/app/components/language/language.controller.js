(function() {
    'use strict';

    angular
        .module('dmtoolsApp')
        .controller('DMTLanguageController', DMTLanguageController);

    DMTLanguageController.$inject = ['$translate', 'DMTLanguageService', 'tmhDynamicLocale'];

    function DMTLanguageController ($translate, DMTLanguageService, tmhDynamicLocale) {
        var vm = this;

        vm.changeLanguage = changeLanguage;
        vm.languages = null;

        DMTLanguageService.getAll().then(function (languages) {
            vm.languages = languages;
        });

        function changeLanguage (languageKey) {
            $translate.use(languageKey);
            tmhDynamicLocale.set(languageKey);
        }
    }
})();
