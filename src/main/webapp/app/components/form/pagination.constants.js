(function() {
    'use strict';

    angular
        .module('dmtoolsApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
