(function() {
    'use strict';

    angular
        .module('dmtoolsApp')
        .config(localStorageConfig);

    localStorageConfig.$inject = ['$localStorageProvider', '$sessionStorageProvider'];

    function localStorageConfig($localStorageProvider, $sessionStorageProvider) {
        $localStorageProvider.setKeyPrefix('dmt-');
        $sessionStorageProvider.setKeyPrefix('dmt-');
    }
})();
